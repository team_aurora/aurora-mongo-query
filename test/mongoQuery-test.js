const mongoQuery = require('./../index');
const mongo = require('mongodb').MongoClient;
const dbUrl = 'mongodb://localhost/test';
mongoQuery.setUrl(dbUrl);
const assert = require('chai').assert;


describe('Mongo Queries test', () => {
  before(() => {
    const insertElements = [
      {
        name: 'element1',
        elementId: '12345'
      },
      {
        name: 'element2',
        elementId: '67890'
      },
      {
        name: 'element3',
        elementId: '111111'
      },
      {
        name: 'element4',
        elementId: '0000000'
      },
      {
        name: 'element5',
        elementId: 'thisisaelementid'
      }
    ];
    mongo.connect(dbUrl)
      .then((client) => {
        const elements = client.collection('elements');
        elements.insertMany(insertElements)
        .then(response => {
          client.close();
        });
      });
  });

  after(() => {
    mongo.connect(dbUrl)
      .then((client) => {
        const elements = client.collection('elements');
        elements.remove({})
        .then(response => {
          client.close();
        });
      });
  });

  describe('#get() get multiple records by condition', () => {
    it('Check if the returned array is not empty when exist 1 match value', async () => {
      const elementId = '12345';
      const elements = await mongoQuery.get('elements', { elementId });
      assert.isArray(elements);
      assert.lengthOf(elements, 1);
      assert.equal(elements[0].elementId, elementId);
    });
  
    it('Check if the returned array is not empty when exist 1 or more matches', async () => {
      const condition = {
        $or: [
          { elementId: '12345' },
          { elementId: '67890' }
        ]
      }
      const elements = await mongoQuery.get('elements', condition);
      assert.isArray(elements);
      assert.lengthOf(elements, 2);
    });
  
    it('Check if the returned array is empty when NOT exist matches', async () => {
      const elementId = 'abcde';
      const elements = await mongoQuery.get('elements', { elementId });
      assert.isArray(elements);
      assert.isEmpty(elements);
    });
  });


  describe('#getOne() get one record by condition', () => {
    it('Check if the returned array is not empty when exist 1 match value', async () => {
      const elementId = '12345';
      const element = await mongoQuery.getOne('elements', { elementId });
      assert.isObject(element);
      assert.equal(element.elementId, elementId);
    });
  
    it('Check if the returned array is not empty when exist 1 or more matches', async () => {
      const condition = {
        $or: [
          { elementId: '12345' },
          { elementId: '67890' }
        ]
      }
      const elementId = '12345';
      const element = await mongoQuery.getOne('elements', condition);
      assert.isObject(element);
      assert.equal(element.elementId, elementId);
    });
  
    it('Check if the returned array is empty when NOT exist matches', async () => {
      const elementId = 'abcde';
      const element = await mongoQuery.getOne('elements', { elementId });
      assert.isObject(element);
      assert.isEmpty(element);
    });
  });


  describe('#getAll() get all records by collection name', () => {
    it('Check if the returned array is not empty when exist 1 match value', async () => {
      const elements = await mongoQuery.getAll('elements');
      const elementsLength = 5; //precaulculated
      assert.isArray(elements);
      assert.isNotEmpty(elements);
      assert.lengthOf(elements, elementsLength);
    });
  
    it('Check if the returned array is empty when the collection not exist', async () => {
      const elements = await mongoQuery.getAll('otherCollection');
      assert.isArray(elements);
      assert.isEmpty(elements);
    });
  });
  
});



