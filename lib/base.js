//const q = require('q');
//const async = require('async');

const mongoClient = require('mongodb').MongoClient;
let dbbUrl;

module.exports.setUrl = function setUrl(urlString) {
  dbbUrl = urlString;

  return mongoClient.connect(dbbUrl)
    .then(db => {
      db.close();
      return true
    })
};


module.exports.get = function get(table, condition, aggregate = false, url = dbbUrl) {
  return mongoClient.connect(url)
    .then(db => {
      const collection = db.collection(table)
      const response = aggregate ? collection.aggregate(condition) : collection.find(condition);
      return response.toArray()
        .then(queryResult => {
          db.close();
          return queryResult;
        }).catch(queryErr => {
          if (queryErr) {
            return queryErr;
          }
        });
    }).catch(err => {
      if (err) {
        console.log("Basemodel get err...", err);
        return err;
      }
    });
}

module.exports.getOne = function getOne(table, condition) {

  return mongoClient.connect(dbbUrl)
    .then(db => {
      const collection = db.collection(table)
      return collection.findOne(condition)
        .then(queryResult => {
          queryResult = !queryResult ? {} : queryResult;
          db.close();
          return queryResult;
        }).catch(queryErr => {
          if (queryErr) {
            return queryErr;
          }
        });
    }).catch(err => {
      if (err) {
        console.log("Basemodel getOne err...", err);
        return err;
      }
    });
}

// module.exports.getCompanyByElementId = function getOne(elementId) {

//   var defer = q.defer();


//   mongoClient.connect(dbbUrl, function (err, db) {

//     if (err) {
//       console.log("Basemodel getCompanyByElementId err...", err)
//       return defer.reject(err)
//     }

//     var elementCollection = db.collection('elements');
//     var companyCollection = db.collection('companies');

//     var regExpression = new RegExp("/" + elementId + "/g");

//     async.waterfall([

//       (cb) => {
//         elementCollection.findOne({ elementId: { $regex: regExpression } }).toArray(function (queryErr, queryResult) {

//           if (queryErr) {
//             console.log("queryErr...", queryErr)
//             cb(err)
//           }

//           cb(null, queryResult)

//         })
//       },
//       (elementObj, cb) => {

//         elementCollection.findOne({ _id: elementObj.company }).toArray(function (queryErr, queryResult) {

//           if (queryErr) {
//             console.log("queryErr...", queryErr)
//             cb(err)
//           }

//           cb(null, queryResult)

//         })
//       }


//     ], (err, result) => {

//       db.close()

//       if (err)
//         return defer.reject(err)

//       defer.resolve(result)
//     })

//   });

//   return defer.promise;
// }

module.exports.getAll = function getAll(table) {
  return mongoClient.connect(dbbUrl)
    .then(db => {
      const collection = db.collection(table);
      return collection.find()
        .toArray()
        .then(queryResult => {
          db.close()
          return queryResult;
        }).catch(queryErr => {
          if (queryErr) {
            return queryErr;
          }
        });
    }).catch(err => {
      if (err) {
        console.log("Basemodel getAll err...", err);
        return err;
      }
    });
}