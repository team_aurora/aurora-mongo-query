# aurora-mongo-query Package #


## Objective ##

**aurora-mongo-query package** provides utilities for mongoDB queries.

## Installation ##

### Usage ###

* Install the package
```
#!javascript
npm install https://bitbucket.org/team_aurora/aurora-mongo-query.git --save
```
* Define the url database string

```
#!javascript
const urlString = 'mongodb://localhost/test';
```
* Run mongoDB with a database named "test"

* Require it in your code
```
#!javascript
const urlString = 'mongodb://localhost/test';
const mongoQuery = require('aurora-mongo-query');
mongoQuery.setUrl(urlString);
```

### Unit Test ###

```
#!javascript

npm test
```